# -*- coding: utf-8 -*-
"""
@author: CY Peng

DataVer  - Author - Note
2019/05/08      CY   Preprocessing
2019/05/15      CY   Training/Evaluation/ Modulization
2019/05/22      CY   Transfer Learning OK
2019/10/30      CY   add the Model Type Choice
"""
# Solution of the GPU Issue
# Ref
# https://blog.csdn.net/qq_41185868/article/details/79127838
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import tensorflow as tf
import CommonUtility as CU
import numpy as np
import tf_mnist_preprocessing as PrePro
import LeNet as LN
import glob
import shutil
from shutil import copyfile

from tensorflow.python.framework import graph_util
from tensorflow.python.tools import freeze_graph
import time
import NetworkLib as NL

def training_stage(sess, algObj, dataset_x, dataset_y, batch_size):
    for offset in range(0, dataset_x.shape[0], batch_size):
        end = offset + batch_size
        batch_x, batch_y = dataset_x[offset:end], dataset_y[offset:end]
        #print(batch_x, batch_y)
        sess.run([algObj.training_operation], feed_dict={algObj.x: batch_x, algObj.y: batch_y})

def evaluation_stage(sess,
                     feed_x,
                     feed_y,
                     loss_operation,
                     accuracy_operation,
                     dataset_x,
                     dataset_y,
                     this_epoch,
                     batch_size,
                     writer = None):
    run_loss, run_accuracy, count = 0, 0, 0
    for offset in range(0, dataset_x.shape[0], batch_size):
        end = offset + batch_size
        batch_x, batch_y = dataset_x[offset:end], dataset_y[offset:end]
        loss, accuracy = sess.run([loss_operation, accuracy_operation],
                                  feed_dict={feed_x: batch_x, feed_y: batch_y})
        if writer is not None:
            summary = tf.Summary(value=[
                tf.Summary.Value(tag="loss_val", simple_value=loss),
                tf.Summary.Value(tag="accuracy_val", simple_value=accuracy),
            ])
            writer.add_summary(summary, this_epoch)
        run_loss += loss
        run_accuracy += accuracy
        count += 1
        print("EPOCH %d BatchSizeOffset %d: Loss: %f, Accuracy: %f" %
              (this_epoch, offset, loss, accuracy))
    run_loss /= count
    run_accuracy /= count
    print("EPOCH %d: Loss: %f, Accuracy: %f" %
          (this_epoch, run_loss, run_accuracy))
    return run_loss, run_accuracy

def ModelValidation(validation_db_filename,
                    model_base_folder,
                    shuffle_size,
                    repeat_num,
                    batch_size,
                    total_validation_sample_num=1000):
    # Validation - Dataset Preparation
    model_folder = model_base_folder + "\\training\\"
    if not os.path.isdir(model_folder):
        model_folder = model_base_folder + "\\translearning_training\\"
    val_meta_file = model_folder + "opt\\opt_model.ckpt.meta"

    # Model Reading
    val_saver = tf.train.import_meta_graph(val_meta_file, clear_devices=True)

    with tf.Session() as val_sess:
        val_saver.restore(val_sess, tf.train.latest_checkpoint(model_folder))
        #print(tf.all_variables())

        graph = tf.get_default_graph()
        x = graph.get_tensor_by_name("input/x:0")
        y = graph.get_tensor_by_name("output/y:0")
        loss_op = tf.get_collection("loss_operation")[0]
        accuracy_op = tf.get_collection("accuracy_operation")[0]
        #print(x,y, loss_op, accuracy_op)

        # Validation
        print("Validation Start...")
        image_validation_dataset, label_validation_dataset = PrePro.TFRecordOneShot2BacthDataset(validation_db_filename,
                                                                                                 total_validation_sample_num,
                                                                                                 shuffle_size,
                                                                                                 repeat_num,
                                                                                                 batch_size)

        evaluation_stage(val_sess, x, y, loss_op, accuracy_op, image_validation_dataset,
                         label_validation_dataset, 0, batch_size, writer=None)

        builder = tf.saved_model.builder.SavedModelBuilder(model_base_folder+"\\validation\\")
        builder.add_meta_graph_and_variables(val_sess,
                                             [tf.saved_model.tag_constants.TRAINING],
                                             signature_def_map=None,
                                             assets_collection=None)
        builder.save()
        val_sess.close()
        print("Validation End...")

def restore_part_variables(meta_dir):
    reader = tf.train.NewCheckpointReader(meta_dir + "model.ckpt")
    modvarlist, glovarlist, revarlist, inivarlist = [], [], [], []
    var_to_shape_map = reader.get_variable_to_shape_map()
    glovar = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES)
    for key in sorted(var_to_shape_map):
        modvarlist.append(key)
    for idx in range(0, len(glovar)):
        glovarlist.append(glovar[idx].name.split(":0")[0])
    for idx in range(0, len(glovar)):
        var_name = glovarlist[idx]
        if var_name in modvarlist:
            revarlist.append(glovar[idx])
        else:
            inivarlist.append(glovar[idx])
    return revarlist, inivarlist

def TFRecord2TraningLeNetModel(training_db_filename,
                               test_db_filename,
                               epoch_num,
                               shuffle_size,
                               repeat_num,
                               batch_size,
                               meta_dir =  ".\\output\\model_0\\training\\",
                               output_dir = ".\\output\\",
                               total_training_sample_num = 2000,
                               total_test_sample_num = 1000,
                               learning_rate = 0.001):

    # Output Folder Check
    model_output_folder = CU.updateOutputFolder(output_dir, outputfolder="model_")
    output_base_folder = model_output_folder + "\\training\\"
    ckpt_filename = output_base_folder + "model.ckpt"
    opt_ckpt_filename = output_base_folder + "\\opt\\opt_model.ckpt"
    log_dir = output_base_folder + "train_logs\\"

    # Mode Initialization
    opt_accuracy, opt_epoch = 0, 0
    LeNetAlg = LN.LeNet(learning_rate = learning_rate, Mode = "default_unit_model_load")
    LeNetAlg.set_tran_variable_scope(var_list=None)

    # Variables Initialization
    var_ini_operation = [tf.global_variables_initializer(), tf.local_variables_initializer()]

    # Training Obejct Saver Establishment
    saver = tf.train.Saver()

    with tf.Session() as train_sess:
        train_sess.run(var_ini_operation)
        train_writer = tf.summary.FileWriter(log_dir, train_sess.graph)
        tf.train.write_graph(train_sess.graph.as_graph_def(), model_output_folder, 'model.pbtxt', as_text=True)
        # graph = tf.graph_util.convert_variables_to_constants(sess, sess.graph_def, ["output"])
        tf.train.write_graph(train_sess.graph.as_graph_def(), model_output_folder, 'model.pb', as_text=False)
        for epoch_now in range(0, epoch_num):
            print("EPOCH {} Start...".format(epoch_now + 1))
            # Dataset Preparation
            image_training_dataset, label_training_dataset = PrePro.TFRecordOneShot2BacthDataset(training_db_filename,
                                                                                                 total_training_sample_num,
                                                                                                 shuffle_size,
                                                                                                 repeat_num,
                                                                                                 batch_size)

            image_test_dataset, label_test_dataset = PrePro.TFRecordOneShot2BacthDataset(test_db_filename,
                                                                                         total_test_sample_num,
                                                                                         shuffle_size,
                                                                                         repeat_num,
                                                                                         batch_size)

            # print(image_dataset.shape, label_dataset.shape[0])
            # Training
            training_stage(train_sess, LeNetAlg, image_training_dataset, label_training_dataset, batch_size)

            # Testing
            loss, accuracy = evaluation_stage(train_sess,
                                              LeNetAlg.x,
                                              LeNetAlg.y,
                                              LeNetAlg.loss_operation,
                                              LeNetAlg.accuracy_operation,
                                              image_test_dataset,
                                              label_test_dataset,
                                              epoch_now + 1,
                                              batch_size,
                                              writer=train_writer)

            saver.save(train_sess, ckpt_filename, global_step=epoch_now)
            if epoch_now == 0:
                opt_train_sess = train_sess
            if opt_accuracy < accuracy:
                opt_accuracy = accuracy
                opt_train_sess = train_sess
                opt_epoch = epoch_now
            saver.save(opt_train_sess, opt_ckpt_filename)
            print("Optimization Epoch {} for Accuracy {}".format(opt_epoch + 1, opt_accuracy))
        saver.save(train_sess, ckpt_filename)
        train_sess.close()
        train_writer.close()

        print("Model saved")
        print('All Threads Are Stopped!')

def TFRecord2TranslearningLeNetModel(training_db_filename,
                                     test_db_filename,
                                     epoch_num,
                                     shuffle_size,
                                     repeat_num,
                                     batch_size,
                                     mode = "recovering",
                                     meta_dir =  ".\\output\\model_0\\training\\",
                                     output_dir = ".\\output\\",
                                     total_training_sample_num = 2000,
                                     total_test_sample_num = 1000,
                                     learning_rate = 0.001):
    # Output Folder Check
    # meta_filename = meta_dir + "model.ckpt.meta"
    model_output_folder = CU.updateOutputFolder(output_dir, outputfolder="model_")
    output_base_folder = model_output_folder + "\\translearning_training\\"
    ckpt_filename = output_base_folder + "model.ckpt"
    old_ckpt_filename = meta_dir + "\\opt\\opt_model.ckpt"
    opt_ckpt_filename = output_base_folder + "\\opt\\opt_model.ckpt"
    log_dir = output_base_folder + "train_logs\\"

    # Model Initialization
    opt_accuracy, opt_epoch = 0, 0
    if mode is "recovering":
        train_var_scope_list = ["dense3"]
        LeNetAlg = LN.LeNet(learning_rate = learning_rate, Mode = "default_unit_model_load")
    elif mode is "translearn":
        train_var_scope_list = ["new_dense3"]
        LeNetAlg = LN.LeNet(learning_rate = learning_rate, Mode="translearn")
    else:
        return "Mode Issues!"

    # print(set(tf.all_variables()))
    LeNetAlg.set_tran_variable_scope(var_list=train_var_scope_list)

    # Variables Initialization
    var_ini_operation = [tf.global_variables_initializer(), tf.local_variables_initializer()]

    # Training Obejct Saver Establishment
    restore_var_list, ini_var_list = restore_part_variables(meta_dir)
    old_saver = tf.train.Saver(restore_var_list)
    saver = tf.train.Saver()
    part_var_ini_operation = tf.initialize_variables(ini_var_list)

    with tf.Session() as train_sess:
        train_sess.run(var_ini_operation)
        # old_saver.restore(train_sess, tf.train.latest_checkpoint(meta_dir))
        old_saver.restore(train_sess, old_ckpt_filename)
        train_sess.run(part_var_ini_operation)
        train_writer = tf.summary.FileWriter(log_dir, train_sess.graph)
        tf.train.write_graph(train_sess.graph.as_graph_def(), model_output_folder, 'model.pbtxt', as_text=True)
        # graph = tf.graph_util.convert_variables_to_constants(sess, sess.graph_def, ["output"])
        tf.train.write_graph(train_sess.graph.as_graph_def(), model_output_folder, 'model.pb', as_text=False)
        for epoch_now in range(0, epoch_num):
            print("EPOCH {} Start...".format(epoch_now + 1))
            # Dataset Preparation
            image_training_dataset, label_training_dataset = PrePro.TFRecordOneShot2BacthDataset(
                training_db_filename,
                total_training_sample_num,
                shuffle_size,
                repeat_num,
                batch_size)

            image_test_dataset, label_test_dataset = PrePro.TFRecordOneShot2BacthDataset(test_db_filename,
                                                                                         total_test_sample_num,
                                                                                         shuffle_size,
                                                                                         repeat_num,
                                                                                         batch_size)

            # print(image_dataset.shape, label_dataset.shape[0])
            # Training
            training_stage(train_sess, LeNetAlg, image_training_dataset, label_training_dataset, batch_size)

            # Testing
            loss, accuracy = evaluation_stage(train_sess,
                                              LeNetAlg.x,
                                              LeNetAlg.y,
                                              LeNetAlg.loss_operation,
                                              LeNetAlg.accuracy_operation,
                                              image_test_dataset,
                                              label_test_dataset,
                                              epoch_now + 1,
                                              batch_size,
                                              writer=train_writer)

            saver.save(train_sess, ckpt_filename, global_step=epoch_now)
            if epoch_now == 0:
                opt_train_sess = train_sess
            if opt_accuracy < accuracy:
                opt_accuracy = accuracy
                opt_train_sess = train_sess
                opt_epoch = epoch_now
            saver.save(opt_train_sess, opt_ckpt_filename)
            print("Optimization Epoch {} for Accuracy {}".format(opt_epoch + 1, opt_accuracy))
        saver.save(train_sess, ckpt_filename)
        train_sess.close()
        train_writer.close()

        print("Model saved")
        print('All Threads Are Stopped!')

def model2prediction(input_folder, model_base_folder, model ="default_mode"):
    # Output Folder Check
    model_folder = model_base_folder+"\\validation\\"
    filenamelist = glob.glob(input_folder+"*.png")
    #if os.path.exists(input_folder):
    #    shutil.rmtree(input_folder)
    #os.system("rm -rf " + input_folder)

    # Execution Initialization
    with tf.Session() as pred_sess:
        tf.saved_model.loader.load(pred_sess, [tf.saved_model.tag_constants.TRAINING], model_folder)

        graph = tf.get_default_graph()
        x = graph.get_tensor_by_name("input/x:0")
        if model is "default_mode":
            prediction = tf.argmax(graph.get_tensor_by_name("dense3/output:0"), 1)
        elif model is "translearn_mode":
            prediction = tf.argmax(graph.get_tensor_by_name("new_dense3/output:0"), 1)
        else:
            return "Model Issues!"

        for idx in range(0, len(filenamelist)):
            image_array = PrePro.Images2array(filenamelist[idx])
            pred_val = pred_sess.run(prediction[0], feed_dict={x: image_array})
            os.rename(filenamelist[idx], input_folder + "{}_pred_{}.png".format(idx,pred_val))
        pred_sess.close()
    print("Prediction Completed!")
