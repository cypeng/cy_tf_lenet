# -*- coding: utf-8 -*-
"""
@author: CY Peng

DataVer  - Author - Note
2019/08/07   CY    Initialization
2019/08/13   CY    TensorFlow Solution
2019/10/30   CY    DL Solution Check
2019/11/27    CY     Add CMD Mode and IDE Mode, Path Update, Check
2019/12/01   CY   Second Release (20191201-R 1.0.0)
"""
import os
import sys
import kaggle_mnist_preprocessing as mnist_db
import kaggle_mnist_model_creator as mnist_model
import tf_mnist_model_creator as tf_mnist_model
from configparser import ConfigParser
import re

# TFRecord File Setting
kaggle_database_dir = os.path.join(".", "kaggle_database") #".\\kaggle_database\\"
#database_dir = os.path.join(".", "database")
#KAGGLE_TFRECORD_TRAIN_FILE = os.path.join(kaggle_database_dir, 'mnist_dataset_train')
#KAGGLE_TFRECORD_TEST_FILE = os.path.join(kaggle_database_dir, 'mnist_dataset_test') #database_dir + 'mnist_dataset_test'
#KAGGLE_TFRECORD_VALIDATE_FILE = os.path.join(kaggle_database_dir, 'mnist_dataset_test') #database_dir + 'mnist_dataset_test'
TFRECORD_TRAIN_FILE = os.path.join(kaggle_database_dir, 'mnist_dataset_train') #database_dir + 'mnist_dataset_train'
TFRECORD_TEST_FILE = os.path.join(kaggle_database_dir, 'mnist_dataset_test') #database_dir + 'mnist_dataset_test'
TFRECORD_VALIDATE_FILE = os.path.join(kaggle_database_dir, 'mnist_dataset_test') #database_dir + 'mnist_dataset_test'
model_dest_dir = ".\\lenet\\"
pred_dir = ".\\pred\\"
TRAIN_EPOCH_NUM = 10
TRAIN_SHUFFLE_SIZE = 500
TRAIN_REPEAT_NUM = 1
TRAIN_BATCH_SIZE = 128
TRANSTRAIN_EPOCH_NUM = 10
TRANSTRAIN_SHUFFLE_SIZE = 500
TRANSTRAIN_REPEAT_NUM = 1
TRANSTRAIN_BATCH_SIZE = 128

def set_mnist_database(json_file_path = '.',
                       kaggle_csv_file = "train.csv",
                       database_folder = kaggle_database_dir,
                       train_file = "mnist_dataset_train",
                       test_file = "mnist_dataset_test",
                       augment_ratio = 1.0):
    mnist_db.set_kaggle_mnist_data(json_file_path = json_file_path)
    mnist_db.set_kaggle_mnist_tfrecord(kaggle_csv_file,
                                       database_folder,
                                       train_file,
                                       test_file,
                                       augment_ratio = augment_ratio)
    #mnist_db.read_kaggle_mnist_data("D:\\CY\\Project\\Algorithm\\CODProject\\tf_cnnmnist\\kaggle_database\\train.csv")

def ml_kaggle_training_stage(kaggle_csv_file = "train.csv",
                      database_folder = kaggle_database_dir,
                      output_folder = "ML_output"):
    pass
    #mnist_model.ml_kFold_training_testing_stage(kaggle_csv_file, database_folder, output_folder)

def ml_kaggle_prediction_stage(kaggle_csv_file = "test.csv",
                               database_folder = kaggle_database_dir,
                               model_folder = os.path.join(".","ML_output","XGBClassifier_CV"),
                               modelname = "XGBClassifier_CV"):
    pass
    #mnist_model.ml_kaggle_prediction_stage(kaggle_csv_file, database_folder, model_folder, modelname)

def default_tf_model_training_stage(train_file = TFRECORD_TRAIN_FILE,
                                    test_file = TFRECORD_TEST_FILE,
                                    epoch_num = TRAIN_EPOCH_NUM,
                                    shuffle_size = TRAIN_SHUFFLE_SIZE,
                                    repeat_num = TRAIN_REPEAT_NUM,
                                    batch_size = TRAIN_BATCH_SIZE,
                                    output_dir = ".\\lenet\\",
                                    total_training_sample_num=1000,
                                    total_test_sample_num=1000,
                                    learning_rate=0.001):
    tf_mnist_model.TFRecord2TraningLeNetModel(train_file,
                                              test_file,
                                              epoch_num, #*10,
                                              shuffle_size,
                                              repeat_num,
                                              batch_size,
                                              output_dir=output_dir,
                                              total_training_sample_num=total_training_sample_num,
                                              total_test_sample_num=total_test_sample_num,
                                              learning_rate = learning_rate)


def default_tf_model_traning_recovering_stage(train_file = TFRECORD_TRAIN_FILE,
                                              test_file = TFRECORD_TEST_FILE,
                                              epoch_num = TRAIN_EPOCH_NUM,
                                              shuffle_size = TRAIN_SHUFFLE_SIZE,
                                              repeat_num = TRAIN_REPEAT_NUM,
                                              batch_size = TRAIN_BATCH_SIZE,
                                              meta_dir = ".\\lenet\\model_0\\training\\",
                                              output_dir = ".\\lenet\\",
                                              total_training_sample_num=1000,
                                              total_test_sample_num=1000,
                                              learning_rate=0.001):
    tf_mnist_model.TFRecord2TranslearningLeNetModel(train_file,
                                           test_file,
                                           epoch_num, #*2,
                                           shuffle_size,
                                           repeat_num,
                                           batch_size,
                                           meta_dir=meta_dir,
                                           output_dir=output_dir,
                                           total_training_sample_num=total_training_sample_num,
                                           total_test_sample_num=total_test_sample_num,
                                           learning_rate=learning_rate)

def default_tf_model_validation_stage(val_file = TFRECORD_VALIDATE_FILE,
                                      model_base_folder = ".\\lenet\\model_0",
                                      shuffle_size=TRAIN_SHUFFLE_SIZE,
                                      repeat_num=TRAIN_REPEAT_NUM,
                                      batch_size=TRAIN_BATCH_SIZE,
                                      total_validation_sample_num= 500):
    tf_mnist_model.ModelValidation(val_file,
                                   model_base_folder,
                                   shuffle_size,
                                   repeat_num,
                                   batch_size,
                                   total_validation_sample_num=total_validation_sample_num)

def tf_kaggle_prediction_stage(kaggle_pred_csv_file = "test.csv",
                               database_folder = kaggle_database_dir,
                               model_base_folder =".\\lenet\\model_0"):
    mnist_model.tf_kaggle_prediction_stage(kaggle_pred_csv_file,
                                           database_folder,
                                           model_base_folder)

def main():
    mode = check_env()
    if (1 == mode):
        cmd_mode()
    if (2 == mode):
        ide_mode()
    #webcam_application()

def cmd_mode():
    # from ConfigParser import ConfigParser # for python3
    data_file = sys.argv[1]
    config = ConfigParser()
    config.read(data_file)
    kewgs = {}
    for para in config[config['Execution']['scope']]:
        pattern = re.compile(r'^[-+]?[-0-9]\d*\.\d*|[-+]?\.?[0-9]\d*$')
        flag = pattern.match(config[config['Execution']['scope']][para])
        if flag:
            try:
                kewgs[para] = float(config[config['Execution']['scope']][para])
            except:
                pass
            try:
                kewgs[para] = int(config[config['Execution']['scope']][para])
            except:
                pass
        else:
            kewgs[para] = config[config['Execution']['scope']][para]
    """
    for item in config.sections():
        kewgs[item] = {}
        for para in config[item]:
            kewgs[item][para] = config[item][para]
    """
    print('Parameters Setting {}'.format(kewgs))
    if 'set_mnist_database'== config['Execution']['scope']:
        set_mnist_database(**kewgs)
    if 'ml_kaggle_training_stage'== config['Execution']['scope']:
        ml_kaggle_training_stage(**kewgs)
    if 'ml_kaggle_prediction_stage'== config['Execution']['scope']:
        ml_kaggle_prediction_stage(**kewgs)
    if 'default_tf_model_training_stage'== config['Execution']['scope']:
        default_tf_model_training_stage(**kewgs)
    if 'default_tf_model_traning_recovering_stage'== config['Execution']['scope']:
        default_tf_model_traning_recovering_stage(**kewgs)
    if 'default_tf_model_validation_stage'== config['Execution']['scope']:
        default_tf_model_validation_stage(**kewgs)
    if 'tf_kaggle_prediction_stage'== config['Execution']['scope']:
        tf_kaggle_prediction_stage(**kewgs)
    print('Completed!')

def ide_mode():
    set_mnist_database()
    ml_kaggle_training_stage()
    ml_kaggle_prediction_stage()
    default_tf_model_training_stage()  # max objects train:42 test:41
    default_tf_model_traning_recovering_stage()
    default_tf_model_validation_stage()
    tf_kaggle_prediction_stage()

def check_env():
    print('Check environment variables: {}'.format(sys.argv))
    if (len(sys.argv) > 1):
        print("CMD Mode, Author: CY")
        return 1
    else:
        print("IDE Mode, Author: CY")
        return 2

if __name__ == '__main__':
    main()