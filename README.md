# LeNet-5 Model in TensorFlow for MNIST Dataset Digit Recognizer
<!-- blank line -->
<dl>
　<dd>C.Y. Peng</dd>
<!-- blank line -->
　<dd>Keywords: Deep Learning, LeNet-5 Model, TensorFlow Framework, MNIST Digit Recognizer, Kaggle Competition</dd> 
</dl>     


## Lib Information
- Release Ver: 20191201-R 1.0.0
- Lib Ver: 20191201
- Author: C.Y. Peng
- Required Lib: requirements_venv36dl.txt
- OS Required: Windows 64 bit
- [My Kaggle Competition](https://www.kaggle.com/pcyslm)

## Part I. Overview
Implementation of LeNet-5 MNIST dataset digit detector in Tensorflow (tf.nn module).
In this project, we cover several segments as follows:
- [x] Fundemental Principle
   - [x] Introduction to the Covolution Neural Network, CNN
   - [x] LeNet-5 Architecture
   - [x] National Institute of Standards and Technology Database, MNIST Database
- [x] Quick Start
   - [x] Project Lib Architecture
   - [x] Model Established Pipeline
   - [x] Operation Mode
- [x] Other Records
- [x] Reference

LeNet-5 paper is the fundemental CNN in Deep Learning, along side that paper. This repo enables you to have a quick understanding of LeNet-5 Algorithmn for Deep Learning.

## Part II. Fundemental Principle
### Inotroduction to the Covolution Neural Network, CNN
CNN reaches the character recogniton detector based on the shift/scale/distortion invariance features through the local receptive fields, shared weights and subsampling.
LeNet-5 is the fundemental CNN architecture to detect the character.

### LeNet-5 Architecture
According to the reference [1], there are three type layer in the LeNet-5:
- Convolution Layer: The main function is features extraction; there are two features, local receptive fields and shared weights.
- Max Pooling Layer: The main function is subsampling; down the sensitivity of the shift/scale/distortion invariance of the output.
- Fully Connected Layer, FC: Also called multi-layer perceptrons, the main function is classfication or regression, but it has high degree sensitivity of the input

LeNet-5 architecture detail as following figure:
<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/LeNet_5_Architecture.png" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 1. LeNet-5 Architecture [1]
</div>
<!-- blank line -->

There are seven layers in the LeNet-5 as following:     

     No.  Layer              Filters   Size          Padding         Input Layer         Features Map
     1    Convolution        6         5*5/1         SAME            None*32*32*1            28*28      
     2    Max Pooling                  2*2/1         VALID           None*28*28*6            14*14
     3    Convolution        16        5*5/1         VALID           None*14*14*6            10*10
     4    Max Pooling                  2*2/1         VALID           None*10*10*16            5*5
     5    Convolution        120       5*5/1         VALID           None*5*5*16              120
     6    Fully Connected              120*84                        None*1*1*120              84
     7    Fully Connected              84*OutputSize                 None*1*1*84           OutputSize

As Figure 1., the foundemental CNN architecture is using the convolution layer and max pooling layer to extract the low dimensional features, 
and the last layer is the fully connected layer as the features map classfication or regression. To optimize the parameters of the LeNet-5, we use
the tf.nn.softmax_cross_entropy_with_logits function for backpropagation to calculate the softmax cross entropy between logits and labels.
	 
### National Institute of Standards and Technology Database, MNIST Database
The MNIST database of handwritten digits that is commonly used for training/testing various image machine learning processing systems.
In this project, the detector detects the number character 0-9 by training/testing/validating the MNIST database. 	 
	 
## Part III. Quick Start
There are two functions in this project, one is to established the digit recognizer for MNIST datasets, the another is for kaggle competition. 
In this project, we show the several functions of the LeNet-5 from traning model to the validation [2-7] as following figure:
<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/quickStart.png" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 2. LeNet-5 Lib Function Flow
</div>
<!-- blank line -->

First, we use the MNIST datasets to traning the original LeNet-5 to get the 0-9 outputs. Second, we use the MNIST datasets to transfer learning from original LeNet-5 to only 2 outputs (0/1, output: 0 if input < 5, output: 1 if input >= 5).
In kaggle competition, we establihsed the model by these functions.
There are two operation modes in this project: integrated development environment mode (IDE mode) and windows command prompt mode (CMD mode).

### Project Lib Architecture
In this project, we need three librarys:
- tf_cnnmnist: MNIST Recognition Lib
  - tf_mnist_main: main file (python file)
  - tf_mnist_preprocessing: some functions are about images preprocessing (python file)   
  - tf_mnist_model_creator: some functions are about training/ testing/ validation (python file)
  
  - kaggle_mnist_main: main file for kaggle competition, [digit recognition](https://www.kaggle.com/c/digit-recognizer) (python file)
  - kaggle_mnist_preprocessing: some functions are about images preparation and preprocessing (python file)   
  - kaggle_mnist_model_creator: some functions are about training/ testing/ validation (python file)

- CommonLib: Common Utility Tool (Note: Using this library, it is important to setup for some cython files!)
  - TFrecord: TensorFlow TFrecord files reading and writting preprocessing (python file)
 
- DeepLearningLib: Various CNN Architecture Model
  - LeNet: LeNet model (python file)
  - NetworkLib: Some neural networks elements (python file)

### Model Established Pipeline
In this project, there are eights steps for MNIST datasets recognizer established, five steps for kaggle competition datasets recognizer established.

#### Recognizer for MNIST dataset 
- image_preprocessing_stage: downloading the files, save images to the TFrecord and images preprocessing.
- default_model_training_stage: setting epoch number, repeat number, batch size and shuffle size to train/ test the model.
- default_model_traning_recovering_stage: recovering to the traning stage from the last training check point. 
- default_model_validation_stage: validating the model and save it to the freezon model.
- default_model_prediction_stage: using pipeline input to storing images from TFRecord files to the pictures format, and to detected the images by the freezon model.
- binary_model_translearning_stage: setting epoch number, repeat number, batch size and shuffle size to train/ test the model.
- binary_model_validation_stage: validating the model and save it to the freezon model.
- binary_model_prediction_stage: using pipeline input to storing images from TFRecord files to the pictures format, and to detected the images by the freezon model.

#### Recognizer for kaggle competition 
- set_mnist_database: downloading the files from kaggle website, save images to the TFrecord and images preprocessing.
- ml_kaggle_training_stage: under construction.
- ml_kaggle_prediction_stage: under construction.
- default_tf_model_training_stage: setting epoch number, repeat number, batch size and shuffle size to train/ test the model.
- default_tf_model_traning_recovering_stage: recovering to the traning stage from the last training check point. 
- default_tf_model_validation_stage: validating the model and save it to the freezon model.
- tf_kaggle_prediction_stage: using the model to predict the dataset of the kaggle competition, and produced the file for kaggle solution format.

### Operation Mode
#### IDE mode:
Excute the tf_mnist_main.py or kaggle_mnist_main.py in different IDE.
#### CMD mode:
- Set cmd_db_config.txt file for MNIST datasets recognizer established, set cmd_kaggle_config.txt file for kaggle competition datasets recognizer established.
- Excute command line in windows cmd, examples as the enable_db_hw.bat file or the enable_kaggle_cwm.bat file.

## Part V. Other Records
### Some Notes
TensorBoard Command for Terminal:
- tensorboard --logdir=path --host=127.0.0.1

Freezon Model Command for Terminal:
- python freeze_graph.py --input_graph=*.pbtxt --input_checkpoint=*.ckpt --output_graph=*.pb --output_node_names=node_name

If you have any questions or suggestions, please let me know, thank you. Enjoy it!

### History Notes
     DataVer    Author      Note
     20190503   C.Y. Peng   TFRecord Test
     20190505   C.Y. Peng   TFRecord Load/ Read Data OK
     20190508   C.Y. Peng   Preprocessing
     20190515   C.Y. Peng   Training/Evaluation/ Modulization
	 20190522   C.Y. Peng   Transfer Learning OK
	 20190523   C.Y. Peng   Completed
	 20190527   C.Y. Peng   Bug Fixed
	 20190625   C.Y. Peng   Add the Binary Model
	 20190630   C.Y. Peng   First Release
	 20190807   C.Y. Peng   Kaggle Competitions Initialization
     20190813   C.Y. Peng   TensorFlow Solution for Kaggle Competitions 
	 20191030   C.Y. Peng   add the Model Type Choice
     20191127   C.Y. Peng   Add CMD Mode and IDE Mode, Path Update, Check
     20191201   C.Y. Peng   Second Release (20191201-R 1.0.0)

## Part VI. Reference
### Some Official/Papers/Books Reference
- [1] Yann Lecun, Leon Bottou, Yoshua Bengio and Patrick Haffner, "Gradient-Based Learning Applied to Document Recognition, " Proc. of the IEEE, November, 1998.
- [2] [TensorFlow Tourials](https://www.tensorflow.org/tutorials)
- [3] [What’s coming in TensorFlow 2.0](https://medium.com/tensorflow/whats-coming-in-tensorflow-2-0-d3663832e9b8)
- [4] Adam Gibson and Josh Patterson, Deep Learning A Practitioner's Approach, O'Reilly Media, August, 2017
- [5] Geron and Aurelien, Hands-On Machine Learning with Scikit-Learn and TensorFlow: Concepts, Tools, and Techniques to Build Intelligent Systems, O'Reilly Media, April, 2017
- [6] Nick McClure, TensorFlow Machine Learning Cookbook, Packt Publishing, Feburary, 2017
- [7] 鄭澤宇, 梁博文, 顧思宇, TensorFlow實戰Google深度學習框架, 電子工業出版社, 2018

### Some Technique Blog Post
- [LeNet-5, convolutional neural networks](https://blog.csdn.net/nnnnnnnnnnnny/article/details/70216265)
- [Build First Convolutional Neurel Network (CNN)](https://www.ycc.idv.tw/tensorflow-tutorial_3.html)
- [TensorFlow-MNIST-example](https://github.com/pinae/TensorFlow-MNIST-example)
- [Machine Learning - LeNet-5-tensorflow](https://github.com/machine-learning/Lenet-5-tensorflow)
- [A Complete Example of Tensorflow Queues](https://github.com/pemami4911/TF-Queues-Full-MNIST-Example)
- [How to use TFRecord with Datasets and Iterators in Tensorflow with code samples](https://medium.com/ymedialabs-innovation/how-to-use-tfrecord-with-datasets-and-iterators-in-tensorflow-with-code-samples-ffee57d298af)
- [Convert and using the MNIST dataset as TFRecords](https://www.damienpontifex.com/2017/09/18/convert-and-using-the-mnist-dataset-as-tfrecords/)
- [Saving, Freezing, Optimizing for inference, Restoring of tensorflow models](https://medium.com/@prasadpal107/saving-freezing-optimizing-for-inference-restoring-of-tensorflow-models-b4146deb21b5)
- [TensorFlow: saving/restoring and mixing multiple models](https://blog.metaflow.fr/tensorflow-saving-restoring-and-mixing-multiple-models-c4c94d5d7125)
- [A quick complete tutorial to save and restore Tensorflow models](https://cv-tricks.com/tensorflow-tutorial/save-restore-tensorflow-models-quick-complete-tutorial/)
- [[Tensorflow] Remove nodes & add nodes with pre-trained model](https://medium.com/@chenchoulo/tensorflow-remove-nodes-add-nodes-with-pre-trained-model-4fb8cf44b531)
- [Save and Restore a Tensorflow model with its Dataset using simple_save](https://vict0rs.ch/2018/05/17/restore-tf-model-dataset/)
- [Tensorflow tf.data.Dataset](https://zhuanlan.zhihu.com/p/37106443)
- [tfrecords +tfdata，2种方式训练mnist](https://blog.csdn.net/qq_14845119/article/details/79028094)
- [tensorflow中協調器 tf.train.Coordinator 和入隊線程啟動器](https://blog.csdn.net/dcrmg/article/details/79780331)
- [使用 TensorFlow 來做簡單的手寫數字辨識](https://blog.techbridge.cc/2018/01/27/tensorflow-mnist/)
- [tf.data.Dataset使用](https://www.cnblogs.com/hellcat/p/8569651.html#_label6)
- [将MNIST转为TFRecord](https://blog.csdn.net/White_Idiot/article/details/78822060)
- [再探 TFRecord](https://dotblogs.com.tw/shaynling/2017/11/21/162229)
- [大數據競賽平台Kaggle入門，練習手寫數字辨識](http://arbu00.blogspot.com/2017/03/9-kaggle.html)
- [熟悉 MNIST 手寫數字辨識資料集](https://ithelp.ithome.com.tw/articles/10186473)
- [TensorFlow使用MNISTI識別](http://tunm.top/blog/32)
- [Tensorflow如何导出与使用预测图](https://juejin.im/entry/5c259861e51d4535c926777d)
- [TensorFlow保存和恢复模型的方法总结](https://www.yueye.org/2017/summary-of-save-and-restore-models-in-tensorflow.html)
- [Tensorflow关于Dataset的一般操作](https://blog.csdn.net/qq_35976351/article/details/80752535)
- [【TensorFlow动手玩】队列](https://blog.csdn.net/shenxiaolu1984/article/details/53024513)
- [TensorFlow 训练多个loss函数技巧： 训练步骤，冻结部分层（freeze some scopes），从tensorboard检查问题](https://blog.csdn.net/zseqsc_asd/article/details/83308960)
- [tensorflow将训练好的模型freeze,即将权重固化到图里面,并使用该模型进行预测](https://blog.csdn.net/lujiandong1/article/details/53385092)
- [使用 TensorFlow 來做簡單的手寫數字辨識](https://blog.techbridge.cc/2018/01/27/tensorflow-mnist/)
- [tensorflow將訓練好的模型freeze,即將權重固化到圖裡面,並使用該模型進行預測](https://www.itread01.com/content/1550361094.html)
- [tensorflow保存和恢复模型的两种方法介绍](https://zhuanlan.zhihu.com/p/31417693)