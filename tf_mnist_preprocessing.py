# -*- coding: utf-8 -*-
"""

@author: CY Peng

DataVer  - Author - Note
2019/05/08      CY   Preprocessing
2019/12/1       CY   Lib Update
"""
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import tensorflow as tf
import TFRecord as tfr
from tensorflow.examples.tutorials.mnist import input_data
import matplotlib.pyplot as plt
#import matplotlib.image as mpimg
#import scipy.misc
#from PIL import Image
import glob
#import LeNet as LN
import numpy as np
import CommonUtility as CU
import cv2
import kaggle_login as kaggle
import pandas as pd
#from PIL import Image

def __mnist_image_2_tfrecord_format__(filename, idx, images, labels):
    image_raw = images[idx].tostring()
    label = labels[idx]
    rows = images.shape[1]
    cols = images.shape[2]
    channel = images.shape[3]
    img_tfr_format = tf.train.Example(features=tf.train.Features(
            feature={
                'filename': tfr.bytes_feature(filename),
                'height': tfr.int64_feature(rows),
                'width': tfr.int64_feature(cols),
                'channel': tfr.int64_feature(channel),
                'label': tfr.int64_feature(int(label)),
                'image_raw': tfr.bytes_feature(image_raw)
            }))
    return img_tfr_format

def __tfrecord_2_image_format__(dataset):
    # Extract features using the keys set during creation
    features = {
        'filename': tf.FixedLenFeature([], tf.string),
        'height': tf.FixedLenFeature([], tf.int64),
        'width': tf.FixedLenFeature([], tf.int64),
        'channel': tf.FixedLenFeature([], tf.int64),
        'label': tf.FixedLenFeature([], tf.int64),
        'image_raw': tf.FixedLenFeature([], tf.string),
    }
    image_info = tf.parse_single_example(dataset, features)

    image = tf.decode_raw(image_info['image_raw'], tf.uint8)
    image = tf.reshape(image, (28, 28))
    image = tf.cast(image, tf.float32) * (1. / 255) - 0.5
    img_shape = tf.stack([image_info['height'], image_info['width'], image_info['channel']])
    label = tf.cast(image_info['label'], tf.int32)
    filename = tf.decode_raw(image_info['filename'], tf.uint8)  # .decoder('utf-8')

    return [image, img_shape, label]

def mnist_to_tfrecord(mnist_dataset, tfrecord_file_name, save_img_num = -1):
    images = mnist_dataset.images #tf.reshape(, shape=[-1, 28, 28, 1])
    labels = mnist_dataset.labels
    read_num = save_img_num
    max_num = mnist_dataset.num_examples
    #images = np.pad(images, ((0, 0), (2, 2), (2, 2), (0, 0)), 'constant')
    #print(images.shape)

    if (save_img_num > max_num) or (save_img_num == -1):
        read_num = mnist_dataset.num_examples
    tfr.convert_multiple_images(tfrecord_file_name,
                                read_num,
                                __mnist_image_2_tfrecord_format__,
                                images,
                                labels)

def set_tf_mnist_data(data_dir = "MNIST_data//", reshape_set = False, one_hot_set= False):
    mnist_dataset = input_data.read_data_sets(data_dir,
                                              dtype=tf.uint8,
                                              reshape=reshape_set,
                                              one_hot=one_hot_set)
    print('train', mnist_dataset.train.num_examples,
          ',validation', mnist_dataset.validation.num_examples,
          ',test', mnist_dataset.test.num_examples)
    print('train_images : ', mnist_dataset.train.images.shape,
          ' - labels : ', mnist_dataset.train.labels.shape)
    print('validation_images : ', mnist_dataset.validation.images.shape,
          ' - labels : ', mnist_dataset.validation.labels.shape)
    print('test_images : ', mnist_dataset.test.images.shape,
          ' - labels : ', mnist_dataset.test.labels.shape)
    return mnist_dataset

def mnist2TFRecord(destdir, tfrecord_file_train_name, tfrecord_file_test_name, tfrecord_file_validation_name):
    CU.createOutputFolder(destdir)
    filepath = os.path.join(destdir, "*.tfrecords")

    if (len(glob.glob(filepath)) < 3):
        mnist_dataset = set_tf_mnist_data()
        mnist_to_tfrecord(mnist_dataset.train, tfrecord_file_train_name)
        mnist_to_tfrecord(mnist_dataset.test, tfrecord_file_test_name)
        mnist_to_tfrecord(mnist_dataset.validation, tfrecord_file_validation_name)
    print("All Database is Already!")


def TFRecordOneShotPipeline(tfrecord_filename, shuffle_size, repeat_num, batch_size):
    image_info = tfr.one_shot_extract_multiple_images(__tfrecord_2_image_format__,
                                                      tfrecord_filename,
                                                      shuffle_size,
                                                      repeat_num,
                                                      batch_size)
    return image_info

def TFRecordOneShot2BacthDataset(tfrecord_filename, total_num, shuffle_size, repeat_num, batch_size):
    # Extracting the Images
    image_info = tfr.one_shot_extract_multiple_images(__tfrecord_2_image_format__,
                                                      tfrecord_filename,
                                                      shuffle_size,
                                                      repeat_num,
                                                      batch_size)

    # Initialization
    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    sess.run(tf.local_variables_initializer())

    # Coordinator Initialization
    coord = tf.train.Coordinator()

    # Initialization
    IniFlag = True
    image_dataset = None
    label_dataset = None

    for idx in range(0, total_num, batch_size):
        if not coord.should_stop():
            image, img_shape, label = sess.run(image_info)
            #print(image, img_shape, label)
            try:
                image = np.reshape(image, (batch_size, 28, 28, 1))
            except:
                break
            if (IniFlag):
                image_dataset = image
                label_dataset = label
                IniFlag = False
            else:
                image_dataset = np.concatenate((image_dataset, image), axis=0)
                label_dataset = np.concatenate((label_dataset, label), axis=0)
            #print(image_dataset.shape, label_dataset.shape)

    sess.close()
    return image_dataset, label_dataset

"""
Call Old Version Input Pipeline Function
Old Version: Will be Removed from the New Lib
capacity: An integer. The maximum number of elements in the queue.
capacity=(min_after_dequeue+(num_threads+a small safety margin∗batchsize)
min_after_dequeue: Minimum number elements in the queue after a
dequeue(出列), used to ensure a level of mixing of elements.
"""
def TFRecordInputPipelineBacthDataset(tfrecord_filename, epoch_num, threads_num, capacity_num, batch_size,
                                       min_after_dequeue_num):
    image, img_shape, label = tfr.extract_multiple_images(__tfrecord_2_image_format__,
                                                          tfrecord_filename,
                                                          epoch_num,
                                                          threads_num,
                                                          capacity_num,
                                                          batch_size,
                                                          min_after_dequeue_num)


    image = tf.reshape(image,[batch_size, 28, 28, 1])
    return [image, img_shape, label]

def TFRecordPipelineImagesStoring(tfrecord_filename, total_num, *func_args):

    # Extracting the Images
    image_info = tfr.one_shot_extract_multiple_images(__tfrecord_2_image_format__,
                                                      tfrecord_filename,
                                                      func_args[1],
                                                      func_args[2],
                                                      func_args[3])

    # Initialization
    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    sess.run(tf.local_variables_initializer())

    # Coordinator Initialization
    coord = tf.train.Coordinator()
    epoch_now = 0
    print("All Images Conversion and Saving....Start")

    # Keep extracting data till TFRecord is exhausted
    for idx in range(0, total_num, int(func_args[3])):
        if not coord.should_stop():
            image, img_shape, label = sess.run(image_info)
            for jdx in range(0, func_args[3]):
                plt.imsave(func_args[0]+str(epoch_now)+str(jdx)+'.png', image[jdx])
                #scipy.misc.toimage(image[jdx]).save(func_args[0]+str(epoch_now)+str(jdx)+'.png')
                #mpimg.imsave(func_args[0]+str(epoch_now)+str(jdx), image[jdx])
            epoch_now += 1
    print('Completed!')

def TFRecord2ImagesStoring(destdir, tfrecord_filename, total_sample_num, shuffle_size, repeat_num, batch_size):
    # Preprocessing
    CU.createOutputFolder(destdir)

    # Save Images
    TFRecordPipelineImagesStoring(tfrecord_filename, total_sample_num, destdir, shuffle_size, repeat_num, batch_size)

def Images2array(filename):
    #filenamelist = glob.glob(".\\input\\" + "*.png")
    #filename = filenamelist[0]
    img_raw = cv2.imread(filename, 0)
    image_array = np.array(img_raw, dtype='uint8')
    image_array = image_array * (1. / 255) - 0.5
    image_array = np.reshape(image_array, (1, 28, 28, 1))
    return image_array
