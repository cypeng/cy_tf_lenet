# -*- coding: utf-8 -*-
"""
@author: CY Peng

DataVer  - Author - Note
2019/05/03      CY   TFRecord Test
2019/05/05      CY   TFRecord Load/ Read Data OK
2019/05/08      CY   Preprocessing
2019/05/15      CY   Training/Evaluation/ Modulization
2019/05/22      CY   Transfer Learning OK
2019/05/23      CY   Completed
2019/05/27      CY   Bug Fixed
2019/06/25      CY   Add the Binary Model
2019/06/30      CY   First Release (20190630-R 1.0.0)
2019/10/30      CY   add the Model Type Choice
2019/11/27      CY   Add CMD Mode and IDE Mode, Path Update, Check
2019/12/01      CY   Second Release (20191201-R 1.0.0)
"""
import os
import sys
from configparser import ConfigParser
import tf_mnist_preprocessing as mnist_db
import tf_mnist_model_creator as mnist_model
import re

# TFRecord File Setting
database_dir = os.path.join(".", "database")
TFRECORD_TRAIN_FILE = os.path.join(database_dir, 'mnist_dataset_train')
TFRECORD_TEST_FILE = os.path.join(database_dir, 'mnist_dataset_test') #database_dir + 'mnist_dataset_test'
TFRECORD_VALIDATE_FILE = os.path.join(database_dir, 'mnist_dataset_test') #database_dir + 'mnist_dataset_test'
model_dest_dir = ".\\lenet\\"
pred_dir = ".\\pred\\"
TRAIN_EPOCH_NUM = 10
TRAIN_SHUFFLE_SIZE = 500
TRAIN_REPEAT_NUM = 1
TRAIN_BATCH_SIZE = 128
TRANSTRAIN_EPOCH_NUM = 5
TRANSTRAIN_SHUFFLE_SIZE = 500
TRANSTRAIN_REPEAT_NUM = 1
TRANSTRAIN_BATCH_SIZE = 128

def image_preprocessing_stage(database_dir=database_dir,
                              train_file=TFRECORD_TRAIN_FILE,
                              test_file=TFRECORD_TEST_FILE,
                              val_file=TFRECORD_VALIDATE_FILE):
    mnist_db.mnist2TFRecord(database_dir,
                            train_file,
                            test_file,
                            val_file)

def default_model_training_stage(train_file = TFRECORD_TRAIN_FILE,
                                 test_file = TFRECORD_TEST_FILE,
                                 epoch_num = TRAIN_EPOCH_NUM,
                                 shuffle_size = TRAIN_SHUFFLE_SIZE,
                                 repeat_num = TRAIN_REPEAT_NUM,
                                 batch_size = TRAIN_BATCH_SIZE,
                                 output_dir = ".\\lenet\\",
                                 total_training_sample_num=1000,
                                 total_test_sample_num=1000,
                                 learning_rate=0.001):
    mnist_model.TFRecord2TraningLeNetModel(train_file,
                                           test_file,
                                           epoch_num,
                                           shuffle_size,
                                           repeat_num,
                                           batch_size,
                                           output_dir=output_dir,
                                           total_training_sample_num=total_training_sample_num,
                                           total_test_sample_num=total_test_sample_num,
                                           learning_rate = learning_rate)

def default_model_traning_recovering_stage(train_file=TFRECORD_TRAIN_FILE,
                                            test_file=TFRECORD_TEST_FILE,
                                            epoch_num=TRAIN_EPOCH_NUM,
                                            shuffle_size=TRAIN_SHUFFLE_SIZE,
                                            repeat_num=TRAIN_REPEAT_NUM,
                                            batch_size=TRAIN_BATCH_SIZE,
                                            meta_dir=".\\lenet\\model_0\\training\\",
                                            output_dir=".\\lenet\\",
                                            total_training_sample_num=1000,
                                            total_test_sample_num=1000,
                                            learning_rate=0.001):

    mnist_model.TFRecord2TranslearningLeNetModel(train_file,
                                           test_file,
                                           epoch_num,
                                           shuffle_size,
                                           repeat_num,
                                           batch_size,
                                           meta_dir=meta_dir,
                                           output_dir=output_dir,
                                           total_training_sample_num=total_training_sample_num,
                                           total_test_sample_num=total_test_sample_num,
                                           learning_rate=learning_rate)

def default_model_validation_stage(val_file = TFRECORD_VALIDATE_FILE,
                                    model_base_folder = ".\\lenet\\model_0",
                                    shuffle_size=TRAIN_SHUFFLE_SIZE,
                                    repeat_num=TRAIN_REPEAT_NUM,
                                    batch_size=TRAIN_BATCH_SIZE,
                                    total_validation_sample_num= 500):
    mnist_model.ModelValidation(val_file,
                                model_base_folder,
                                shuffle_size,
                                repeat_num,
                                batch_size,
                                total_validation_sample_num=total_validation_sample_num)

def default_model_prediction_stage(pred_dir = pred_dir,
                                   val_file=TFRECORD_VALIDATE_FILE,
                                   shuffle_size=TRANSTRAIN_SHUFFLE_SIZE,
                                   repeat_num=TRANSTRAIN_REPEAT_NUM,
                                   batch_size=10,
                                   total_validation_sample_num=20,
                                   model_base_folder = ".\\lenet\\model_1"):
    mnist_db.TFRecord2ImagesStoring(pred_dir,
                                    val_file,
                                    total_validation_sample_num,
                                    shuffle_size,
                                    repeat_num,
                                    batch_size)

    mnist_model.model2prediction(pred_dir, model_base_folder)

def binary_model_translearning_stage(train_file=TFRECORD_TRAIN_FILE,
                                    test_file=TFRECORD_TEST_FILE,
                                    epoch_num=TRAIN_EPOCH_NUM,
                                    shuffle_size=TRAIN_SHUFFLE_SIZE,
                                    repeat_num=TRAIN_REPEAT_NUM,
                                    batch_size=TRAIN_BATCH_SIZE,
                                    mode="translearn",
                                    meta_dir=".\\lenet\\model_0\\training\\",
                                    output_dir=".\\lenet\\",
                                    total_training_sample_num=1000,
                                    total_test_sample_num=1000,
                                    learning_rate=0.001):
    mnist_model.TFRecord2TranslearningLeNetModel(train_file,
                                                 test_file,
                                                 epoch_num,
                                                 shuffle_size,
                                                 repeat_num,
                                                 batch_size,
                                                 mode=mode,
                                                 meta_dir=meta_dir,
                                                 output_dir=output_dir,
                                                 total_training_sample_num=total_training_sample_num,
                                                 total_test_sample_num=total_test_sample_num,
                                                 learning_rate=learning_rate)

def binary_model_validation_stage(val_file = TFRECORD_VALIDATE_FILE,
                                    model_base_folder = ".\\lenet\\model_0",
                                    shuffle_size=TRAIN_SHUFFLE_SIZE,
                                    repeat_num=TRAIN_REPEAT_NUM,
                                    batch_size=TRAIN_BATCH_SIZE,
                                    total_validation_sample_num= 500):
    mnist_model.ModelValidation(val_file,
                                model_base_folder,
                                shuffle_size,
                                repeat_num,
                                batch_size,
                                total_validation_sample_num=total_validation_sample_num)

def binary_model_prediction_stage(pred_dir = pred_dir,
                                   val_file=TFRECORD_VALIDATE_FILE,
                                   shuffle_size=TRANSTRAIN_SHUFFLE_SIZE,
                                   repeat_num=TRANSTRAIN_REPEAT_NUM,
                                   batch_size=10,
                                   total_validation_sample_num=20,
                                   model_base_folder = ".\\lenet\\model_0"):
    mnist_db.TFRecord2ImagesStoring(pred_dir,
                                    val_file,
                                    total_validation_sample_num,
                                    shuffle_size,
                                    repeat_num,
                                    batch_size)
    mnist_model.model2prediction(pred_dir, model_base_folder, model="translearn_mode")

def main():
    mode = check_env()
    if (1 == mode):
        cmd_mode()
    if (2 == mode):
        ide_mode()
    #webcam_application()

def cmd_mode():
    # from ConfigParser import ConfigParser # for python3
    data_file = sys.argv[1]
    config = ConfigParser()
    config.read(data_file)
    kewgs = {}
    for para in config[config['Execution']['scope']]:
        pattern = re.compile(r'^[-+]?[-0-9]\d*\.\d*|[-+]?\.?[0-9]\d*$')
        flag = pattern.match(config[config['Execution']['scope']][para])
        if flag:
            try:
                kewgs[para] = float(config[config['Execution']['scope']][para])
            except:
                pass
            try:
                kewgs[para] = int(config[config['Execution']['scope']][para])
            except:
                pass
        else:
            kewgs[para] = config[config['Execution']['scope']][para]
    """
    for item in config.sections():
        kewgs[item] = {}
        for para in config[item]:
            kewgs[item][para] = config[item][para]
    """
    print('Parameters Setting {}'.format(kewgs))

    if 'image_preprocessing_stage'== config['Execution']['scope']:
        image_preprocessing_stage(**kewgs)
    if 'default_model_training_stage'== config['Execution']['scope']:
        default_model_training_stage(**kewgs)
    if 'default_model_traning_recovering_stage'== config['Execution']['scope']:
        default_model_traning_recovering_stage(**kewgs)
    if 'default_model_validation_stage'== config['Execution']['scope']:
        default_model_validation_stage(**kewgs)
    if 'default_model_prediction_stage'== config['Execution']['scope']:
        default_model_prediction_stage(**kewgs)
    if 'binary_model_translearning_stage'== config['Execution']['scope']:
        binary_model_translearning_stage(**kewgs)
    if 'binary_model_validation_stage'== config['Execution']['scope']:
        binary_model_validation_stage(**kewgs)
    if 'binary_model_prediction_stage' == config['Execution']['scope']:
        binary_model_prediction_stage(**kewgs)

    print('Completed!')

def ide_mode():
    image_preprocessing_stage()
    default_model_training_stage()
    default_model_traning_recovering_stage()
    default_model_validation_stage()  # max objects train:42 test:41
    default_model_prediction_stage()
    binary_model_translearning_stage()
    binary_model_validation_stage()
    binary_model_prediction_stage()

def check_env():
    print('Check environment variables: {}'.format(sys.argv))
    if (len(sys.argv) > 1):
        print("CMD Mode, Author: CY")
        return 1
    else:
        print("IDE Mode, Author: CY")
        return 2

if __name__ == '__main__':
    main()
