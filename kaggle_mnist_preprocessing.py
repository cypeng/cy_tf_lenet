# -*- coding: utf-8 -*-
"""

@author: CY Peng

DataVer  - Author - Note
2019/08/11   CY   Preprocessing
2019/08/13   C.Y. Peng  TensorFlow Solution
"""
# Common Usage
import kaggle_login as kaggle
import numpy as np
import pandas as pd
import CommonUtility as CU
import glob
import random

# TensorFlow
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import TFRecord as tfr
import tensorflow as tf
import tf_mnist_preprocessing as tfPrePro
#import LeNet as LN

# Image Processing
#from PIL import Image
import matplotlib.image as mpimg
import cv2

# ML
from sklearn.preprocessing import OneHotEncoder
from sklearn.model_selection import train_test_split

def set_kaggle_mnist_data(json_file_path = '.'):
    database_folder = ".\\kaggle_database"
    file_list = glob.glob(os.path.join(database_folder, "*.csv"))
    if len(file_list) < 3:
        L = kaggle.kaggle_login(json_file_path = json_file_path)
        os.system('kaggle competitions download -c digit-recognizer' + database_folder)
        L.remove_file()
    print("Database Download Success!")

"""
Public Usage
"""
def read_kaggle_mnist_data(filename):
    data = pd.read_csv(filename, index_col = 1, sep=',')
    image_array = np.array(data, dtype='uint8')
    label_array = np.array(data['label'], dtype='uint8') #pd.get_dummies(data['label'])

    return image_array, label_array

def feature_engineering(image_array):
    image_array = image_array * (1. / 255) - 0.5
    image_array = np.reshape(image_array, (len(image_array), 28, 28, 1))
    return image_array

def label_encoder(labels):
    label_array = OneHotEncoder(labels)
    return label_array

"""
TensorFlow Format
"""
def equalRatio2DesiredSize(raw_shape, desired_size= 28):
    ratio = float(desired_size) / max(raw_shape)
    xn, yn = tuple([int(x * ratio) for x in raw_shape])
    xr2t, yr2t = xn / raw_shape[0], yn / raw_shape[1]
    shape_ratio = (xr2t, yr2t)
    new_shape = (xn, yn)

    return new_shape, shape_ratio

def getImageOffset(new_shape, desired_size= 28):
    delta_w = desired_size - new_shape[0]
    delta_h = desired_size - new_shape[1]
    offset_top, offset_bottom = delta_h // 2, delta_h - (delta_h // 2)
    offset_left, offset_right = delta_w // 2, delta_w - (delta_w // 2)
    offset = (offset_top, offset_bottom, offset_left, offset_right)

    return offset

def croppingImages2array(image_raw, desired_size= 28, crop=0.99, angle=10, color = 0.05):
    # Shape Ratio
    #image_raw = cv2.imread(filename, 3).astype(np.float)
    #image_raw_array = np.array(image_raw, dtype='uint8')
    yr, xr = image_raw.shape[:2]
    raw_shape = (xr, yr)

    # rotate
    rot_angle = random.uniform(-angle, +angle)
    Crot = cv2.getRotationMatrix2D((raw_shape[0] / 2, raw_shape[1] / 2), rot_angle, 1)
    image_raw = cv2.warpAffine(image_raw, Crot, raw_shape)

    # crop
    cropping_size = int(min(raw_shape[0], raw_shape[1]) * random.uniform(crop, 1))
    image_x_min = int(random.uniform(0, raw_shape[0] - cropping_size))
    image_y_min = int(random.uniform(0, raw_shape[1] - cropping_size))
    image_x_max = image_x_min + cropping_size
    image_y_max = image_y_min + cropping_size
    #image_limit_shape = (image_x_min, image_y_min, image_x_max, image_y_max)
    image_raw = image_raw[image_y_min:image_y_max, image_x_min:image_x_max]

    # flip
    """
    flip_para = random.random() < 0.5
    if flip_para:
        image_raw = cv2.flip(image_raw, 1)
    """

    # color
    col = random.uniform(1 - color, 1 + color)
    image_raw = image_raw * col
    image_raw[image_raw < 0] = 0
    image_raw[image_raw > 255] = 255

    # resize to inputsize
    new_shape, shape_ratio = equalRatio2DesiredSize((image_x_max-image_x_min, image_y_max-image_y_min),
                                                    desired_size=desired_size)

    # BGR Reshape Image Array
    image_raw = cv2.resize(image_raw, new_shape)
    offset = getImageOffset(new_shape, desired_size=desired_size)

    color = [0, 0, 0]
    resize_image_raw = cv2.copyMakeBorder(image_raw, offset[0], offset[1], offset[2], offset[3],
                                          cv2.BORDER_CONSTANT, value=color)

    image_array = np.reshape(np.array(resize_image_raw, dtype='uint8'), (1, 28, 28, 1))

    return image_array


def images_data_augment(X, y, augment_ratio = 1):
    new_X = np.zeros((X.shape[0]*(augment_ratio+1), X.shape[1], X.shape[2], X.shape[3]), dtype='uint8')
    new_y = np.zeros((y.shape[0]*(augment_ratio+1)), dtype='uint8')
    #print(X.shape, new_X.shape, y.shape, new_y.shape)
    new_idx = 0
    for idx in range(0, len(X)):
        for jdx in range(0, augment_ratio):
            this_new_X = croppingImages2array(X[idx,:,:,:], desired_size=28, crop=0.9, angle=10, color=0.05)
            new_X[new_idx,:,:,:] = this_new_X
            #print(this_new_X.shape)
            new_y[new_idx] = int(y[idx])
            #print(y[idx])
            new_idx += 1
        this_new_X = np.reshape(np.array(X[idx, :, :, :], dtype='uint8'), (1, 28, 28, 1))
        new_X[new_idx, :, :, :] = this_new_X
        new_y[new_idx] = int(y[idx])
        new_idx += 1
    #print(new_X.shape)
    return new_X, new_y

def set_kaggle_mnist_tfrecord(filename, database_folder, tfrecord_train_file_name, tfrecord_test_file_name, augment_ratio = 1):
    training_size = 0.77
    images, labels = read_kaggle_mnist_data(os.path.join(database_folder, filename))
    images = np.reshape(images, (len(images), 28, 28, 1))
    #X, y = images, labels
    #print(X.shape, y.shape)
    X, y = images_data_augment(images, labels, augment_ratio=augment_ratio)
    #print(X.shape, y.shape)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=1 - training_size)

    kaggle_mnist_to_tfrecord(X_train, y_train, database_folder, tfrecord_train_file_name)
    kaggle_mnist_to_tfrecord(X_test, y_test, database_folder, tfrecord_test_file_name)

def kaggle_mnist_to_tfrecord(images, labels, database_folder, tfrecord_file_name, save_img_num = -1):
    read_num = save_img_num
    max_num = len(images)

    if (save_img_num > max_num) or (save_img_num == -1):
        read_num = len(images)

    tfr.convert_multiple_images(os.path.join(database_folder, tfrecord_file_name),
                                read_num,
                                tfPrePro.__mnist_image_2_tfrecord_format__,
                                images,
                                labels)
