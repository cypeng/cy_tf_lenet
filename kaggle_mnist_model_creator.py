# -*- coding: utf-8 -*-
"""
@author: CY Peng

DataVer  - Author - Note
2019/08/13   C.Y. Peng  TensorFlow Solution
"""

# Common Usage
# Solution of the GPU Issue
# Ref
# https://blog.csdn.net/qq_41185868/article/details/79127838

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import kaggle_mnist_preprocessing as PrePro
import pandas as pd
import os
import numpy as np
import CommonUtility as CU

# TensorFlow
import tensorflow as tf
from sklearn.model_selection import train_test_split
from sklearn.model_selection import StratifiedShuffleSplit
#import ClassifierAnalysis as clfAnalysis
import pickle

# Preprocessing
import kaggle_mnist_preprocessing as mnist_db

"""
def ml_kFold_training_testing_stage(filename, database_folder, output_folder):
    output_folder = CU.updateOutputFolder(".\\", outputfolder=output_folder)
    CU.createOutputFolder(output_folder)

    outer_fold_obj = StratifiedShuffleSplit(n_splits=10, random_state=0)
    inner_fold_obj = StratifiedShuffleSplit(n_splits=5, random_state=0)
    training_size = 0.77
    clf = clfAnalysis.ClassifierAnalysis(10, inner_fold_obj, outer_fold_obj, output_folder, "Analysis")

    X, y = mnist_db.read_kaggle_mnist_data(os.path.join(database_folder, filename))
    X = mnist_db.feature_engineering(X)
    X, y = X[0:1000, :], y[0:1000]

    X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=1 - training_size)
    #print(X_train, X_val, y_train, y_val)
    clf.model_training_stage(X_train, y_train, X_val, y_val, opt_num=1)

def ml_kaggle_prediction_stage(filename, database_folder, model_folder, modelname):
    sample_submission = pd.read_csv(os.path.join(database_folder, "sample_submission.csv"))
    data = pd.read_csv(os.path.join(database_folder, filename), sep=',')
    loaded_model = pickle.load(open(os.path.join(model_folder, modelname + ".sav"), 'rb'))

    submission_id = sample_submission["ImageId"]
    X = mnist_db.feature_engineering(np.array(data, dtype='uint8'))

    submission = pd.DataFrame({
        "ImageId": submission_id,
        "Label": loaded_model.predict(X)
    })
    submission.to_csv('submission.csv', index=False)
"""

def tf_kaggle_prediction_stage(filename, database_folder, model_base_folder, model ="default_mode"):
    # Output Folder Check
    model_folder = model_base_folder+"\\validation\\"
    sample_submission = pd.read_csv("submission.csv", sep=',')
    data = pd.read_csv(os.path.join(database_folder, filename), sep=',')
    data = np.array(data, dtype='uint8')

    submission_id = sample_submission["ImageId"]
    answer_array = sample_submission["Label"]

    # Execution Initialization
    with tf.Session() as pred_sess:
        tf.saved_model.loader.load(pred_sess, [tf.saved_model.tag_constants.TRAINING], model_folder)

        graph = tf.get_default_graph()
        x = graph.get_tensor_by_name("input/x:0")
        if model is "default_mode":
            #prediction = tf.argmax(graph.get_tensor_by_name("dense3/output:0"), 1)
            try:
                prediction = tf.argmax(graph.get_tensor_by_name("dense3/output:0"), 1)
            except:
                prediction = tf.argmax(graph.get_tensor_by_name("dense_layer_2/dense_layer_2/layer_out/MatMul:0"), 1)
        elif model is "translearn_mode":
            prediction = tf.argmax(graph.get_tensor_by_name("new_dense3/output:0"), 1)
        else:
            return "Model Issues!"

        image_array = mnist_db.feature_engineering(data)
        answer_array = pred_sess.run(prediction, feed_dict={x: image_array})
        submission = pd.DataFrame({
            "ImageId": submission_id,
            "Label": answer_array
        })
        submission.astype(int)
        submission.to_csv('submission.csv', index=False, float_format="%.0f")
        pred_sess.close()
        del data, answer_array
    print("Prediction Completed!")

